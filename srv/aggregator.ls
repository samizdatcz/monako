require! fs
lines = fs.readFileSync "#__dirname/../data/data.tsv" .toString!split "\n"
lines.shift!
lines.pop!
# lines.length = 10
rocniky = [0 to 65].map -> {}
for line in lines
  [vc_rok, vc_nazev, vc_datum, vc_trat, vc_pocasi, zavod_kola, kolo_delka, zavod_delka, zavod_offset, vuz_cislo, jezdec_jmeno, jezdec_prijmeni, vuz_jezdec, jezdec_aktivni, jezdec_team, vuz_sasi_znacka, vuz_sasi_nazev, vuz_motor_znacka, vuz_motor_nazev, vuz_pneumatiky, bestlap_kolo, bestlap_cas, bestlap_poradi, bestlap_rychlost, bestlap_ztrata, kvalifikace_cas, kvalifikace_ztrata, kvalifikace_rychlost, kvalifikace_prc, kvalifikace_uspech, kvalifikace_poradi, zavod_poradi, jezdec_kola, zavod_cas, zavod_ztrata, zavod_rychlost, vyrazeni_duvod, odstoupeni_zavod, zavod_trest, zavod_body, jezdec_stridani, jezdec_trojka, jezdec_body, jezdec_poradi, tym_body, tym_poradi, sezona_poradi] = line.split "\t"
  year = parseInt vc_rok, 10
  x = year - 1950
  rocnik = rocniky[x]
  # console.log vc_trat
  if rocnik[vc_trat] is void
    rocnik[vc_trat] = {"startovalo": 0, "diskvalifikovano": 0, "kola a brzdy": 0, "motor a palivo": 0, "nehody": 0, "ostatní": 0, "řazení": 0, "šasi": 0}

  zavod = rocnik[vc_trat]
  zavod_poradi_num = parseInt zavod_poradi, 10
  if zavod_poradi_num
    zavod.startovalo++
  if zavod_poradi == "dsq"
    zavod.startovalo++
    zavod.diskvalifikovano++
  if zavod_poradi == "ab"
    zavod.startovalo++
    if odstoupeni_zavod == "elektronika" then odstoupeni_zavod = "ostatní"
    if zavod[odstoupeni_zavod] is void
      console.log odstoupeni_zavod
    zavod[odstoupeni_zavod]++
out = [["rok" "misto" "startovalo" "diskvalifikovano" "kola a brzdy" "motor a palivo" "nehody" "ostatní" "řazení" "šasi"]]
for rocnik, index in rocniky
  year = index + 1950
  for misto, data of rocnik
    out.push [year, misto, data."startovalo", data."diskvalifikovano", data."kola a brzdy", data."motor a palivo", data."nehody", data."ostatní", data."řazení", data."šasi"]

fs.writeFileSync do
  "#__dirname/../data/dojezdy.tsv"
  out
    .map -> it.join "\t"
    .join "\n"

# console.log rocniky
