tracks =
  * name: "Albert Lake"
    id: "Melbourne"
    image: 'albert-lake.svg'
  * name: "Catalunya"
    id: "Barcelone"
    image: 'catalunya.svg'
  * name: "Buenos Aires"
    image: 'galvez.svg'
  * name: "Montréal"
    image: 'gilles_villeneuve.svg'
  * name: "Hockenheim"
    image: 'hokenheimring.svg'
  * name: "Hungaroring"
    image: 'hungaroring.svg'
  * name: "Imola"
    image: 'imola.svg'
  * name: "Indianapolis"
    image: 'indianapolis.svg'
  * name: "Interlagos"
    image: 'interlagos.svg'
  * name: "Kyalami"
    image: 'kyalami.svg'
  * name: "Monaco"
    image: 'monte_carlo.svg'
  * name: "Monza"
    image: 'monza.svg'
  * name: "Nürburgring"
    image: 'nurburgring.svg'
  * name: "Österreichring"
    image: 'red_bull_ring.svg'
  * name: "Silverstone"
    image: 'silverstone.svg'
  * name: "Spa"
    id: "Spa-Francorchamps"
    image: 'spa.svg'
  * name: "Suzuka"
    image: 'suzuka.svg'
  * name: "Watkins Glen"
    image: 'watkins.svg'
  * name: "Zandvoort"
    image: 'zandvoort.svg'
  * name: "Všechny okruhy"
    id: ""
    image: 'spa.svg'

class ig.TrackSelector
  (@parentElement, @callback) ->
    @container = @parentElement.append \div
      ..attr \class "track-selector"
    @initCurrentTrack!
    @list = @container.append \ul
      ..attr \class \hidden
    @listItems = @list.selectAll \li .data tracks .enter!append \li
      ..append \a
        ..attr \class \track
        ..attr \href \#
        ..append \span
          ..attr \class \img-container
          ..append \img
            ..attr \src -> "./img/#{it.image}"
        ..append \span
          ..attr \class \name
          ..html (.name)
        ..on \click ~>
          d3.event.preventDefault!
          @select it
          @hide!
      ..append \span
        ..attr \class \back

  initCurrentTrack: ->
    @currentTrack = @container.append \a
      ..attr \class "track selected"
      ..attr \href \#
      ..append \span
        ..attr \class \img-container
        ..append \img
          ..attr \src -> "./img/spa.svg"
      ..append \span
        ..attr \class \name
        ..html "Všechny okruhy"
      ..on \click ~>
        d3.event.preventDefault!
        @show it


  show: ->
    @list.classed \hidden no
    <~ setTimeout _, 1
    @list.classed \active yes
    @listItems
      ..transition!
        ..delay (d, i) -> 100 + i * 50
        ..attr \class \active

  select: ->
    @selectedItem = it
    @callback if it.id isnt void then it.id else it.name
    @currentTrack.select \span.name .html it.name
    @currentTrack.select \img .attr \src "./img/#{it.image}"

  hide: ->
    @listItems
      ..filter (~> it isnt @selectedItem)
      ..transition!
        ..delay (d, i) -> i * 25
        ..attr \class ""
    <~ setTimeout _, 100
    @list.classed \active no
    <~ setTimeout _, 800
    @list.classed \hidden yes
