technicalReasons = ["kola a brzdy" "motor a palivo" "nehody" "ostatní" "řazení" "šasi"]

allData = d3.tsv.parse ig.data.dojezdy, (row) ->
  for name, value of row
    row[name] = parseInt row[name], 10 if name != "misto"
  row['nedokončilo'] = 0
  for reason in technicalReasons
    row['nedokončilo'] += row[reason]
  row['dojelo'] = row['startovalo'] - row['nedokončilo'] - row['diskvalifikovano']
  row

container = d3.select ig.containers.base
  ..classed \odstoupeni yes
fullWidth = 960
fullHeight = 477

yearOverlay = container.append \div
  ..attr \class \year-overlay
svg = container.append \svg
  ..attr {width:fullWidth, height:fullHeight}
svgDrawing = svg.append \g .attr \class \drawing
svgLayers = svgDrawing.append \g .attr \class \layers
svgAxis = svgDrawing.append \g .attr \class \axis
legend = container.append \ul
  ..attr \class \legend
legendCounts = null

color = d3.scale.ordinal!
    ..range ['rgb(228,26,28)','rgb(55,126,184)','rgb(77,175,74)','rgb(152,78,163)','rgb(255,127,0)','rgb(166,86,40)','rgb(247,129,191)']
xScale = null
yScale = null
area = d3.svg.area!
  ..x  -> xScale it.x
  ..y0 -> yScale it.y0
  ..y1 -> yScale it.y0 + it.y

selectedArea = d3.svg.area!
  ..x  -> xScale it.x
  ..y0 -> yScale 0
  ..y1 -> yScale it.y

updateLegend = (index) ->
  total = 0
  legendCounts.html ->
    if it.reason != "celkem"
      count = it.years[index].count
      total += count
      if 0 < count < 1
        "#{ig.utils.formatNumber count * 100, 1} %"
      else
        ig.utils.formatNumber count
    else
      ig.utils.formatNumber total

resetLegend = ->
  legendCounts.html -> ig.utils.formatNumber it.layerCount

drawOneLayer = (layer) ->
  layers = svgLayers.selectAll \g.layer
  selectedLayer = layers.filter -> it is layer
  otherLayers = layers.filter -> it isnt layer
  otherLayers
    ..transition!
      ..duration 400
      ..attr \opacity 0
  selectedLayer
    ..transition!
      ..attr \opacity 1
      ..duration 600
      ..delay 200
      ..select \path
        ..attr \d -> selectedArea it.yearsStepped


drawAllLayers = ->
  layers = svgLayers.selectAll \g.layer
    ..transition!
      ..duration 600
      ..attr \opacity 1
      ..select \path
        ..attr \d -> area it.yearsStepped

ig.drawOdstoupeni = (pomer, trat) ->
  if pomer
    color := d3.scale.ordinal!
      ..range ['rgb(228,26,28)','#ff7f00','rgb(240,240,255)']
  container.classed \is-pomer pomer
  yearMultipier = 1
  if trat
    data = allData.filter -> it.misto == trat
    if not pomer
      yearMultipier = 5
  else
    data = allData
  container.classed \is-trat yearMultipier > 1

  yearCount = Math.ceil 65 / yearMultipier



  layers_assoc = {}
  reasons = if pomer
    ["nedokončilo" "diskvalifikovano" "dojelo" "startovalo"]
  else
    technicalReasons
  for reason in reasons
    years = [0 to yearCount].map ->
      x: it
      year: it+1950
      count: 0
      datapoints: 0
    layers_assoc[reason] = {years, layerCount: 0}

  totalCount = 0
  for datum in data
    x = datum.rok - 1950
    if yearMultipier > 1
      x = Math.floor x / yearMultipier
    for reason in reasons
      count = datum[reason]
      totalCount += count
      layers_assoc[reason].years[x].datapoints++
      layers_assoc[reason].layerCount += count
      layers_assoc[reason].years[x].count += count

  layers = for reason, {years, layerCount} of layers_assoc
    yearsStepped = []
    for i in [0 til years.length]
      yearObj = years[i]
      if pomer
        yearObj.count /= layers_assoc['startovalo'].years[i].count || 1
      {x, year, count, datapoints} = yearObj
      yearsStepped.push yearObj
      for i in [1 til yearMultipier]
        position = i / yearMultipier
        position *= 0.9
        yearsStepped.push {x: (x + position * 0.9), year, count, datapoints}
        yearsStepped.push {x: x + position, year, count, datapoints}
      x += 0.9
      yearsStepped.push {x, year, count, datapoints}
    {reason, years, yearsStepped, layerCount}
  layers.pop! if pomer
  stack = d3.layout.stack!
    ..x -> it.year
    ..y ->
      if not pomer
        if it.datapoints
          it.count * (yearMultipier / it.datapoints)
        else
          0
      else
        it.count
    ..values (.yearsStepped)
  if not pomer
    stack.order \inside-out

  stack layers
  maxY = -Infinity
  for {years} in layers
    for {y, y0} in years
      yCoord = y + y0
      maxY = yCoord if maxY < yCoord
  margin = top: 100 right: 0px bottom: 40px left: 0px
  if yearMultipier > 1
    margin.top = 180
  if pomer
    margin.top = 0
  width = fullWidth - margin.left - margin.right
  height = fullHeight - margin.top - margin.bottom
  xScale := d3.scale.linear!
    ..domain [0 yearCount]
    ..range [0 width]
  yScale := d3.scale.linear!
    ..domain [0 maxY]
    ..range [height, 0]




  yearOverlay.selectAll \div.year .data [0 til yearCount], (-> it)
    ..enter!append \div
      ..attr \class \year
      ..on \mouseover -> updateLegend it
      ..on \touchstart -> updateLegend it
      ..on \mouseout -> resetLegend it
      ..append \div
        ..attr \class \label
    ..exit!remove!
    ..select \div.label
      ..html ->
        if yearMultipier > 1
          "#{(it * yearMultipier + 1950)} – #{(it + 1) * yearMultipier + 1950}"
        else
          (it * yearMultipier + 1950)

  svgLayers.selectAll \g.layer .data layers, (-> it.reason)
    ..enter!append \g
      ..attr \class \layer
      ..append \path
        ..attr \stroke -> color it.reason
        ..attr \fill -> color it.reason
    ..exit!remove!
    ..select \path
      ..transition!
        ..duration 800
        ..attr \d -> area it.yearsStepped


  svgDrawing.attr \transform "translate(#{margin.left},#{margin.top})"
  svgAxis
    ..attr \transform "translate(0, #{height + 1})"
    ..selectAll \g.tick .data [0 to yearCount]
      ..select \text .remove!
      ..enter!append \g
        ..attr \class \tick
        ..append \line
      ..exit!remove!
      ..attr \transform -> "translate(#{xScale it},0)"
      ..select \line
        ..attr \y2 -> if 0 == it % 10 then 6 else 3
      ..filter (-> yearMultipier > 1 || 0 == it % 10)
        ..append \text
          ..text -> (it * yearMultipier) + 1950
          ..attr \text-anchor ->
            | it == 0 => \start
            | (it * yearMultipier) + 1950 == 2015 => \end
            | otherwise => \middle
          ..attr \y 17


  layers.sort (a, b) -> b.years.10.y0 - a.years.10.y0
  legendLayer = layers.slice!
  legendLayer.push {reason: "celkem", layerCount: totalCount}
  legend
    ..selectAll \li .data legendLayer, (-> "#{it.reason}-#{it.count}")
      ..enter!append \li
        ..append \div .attr \class \reason
        ..append \div .attr \class \count
        ..append \div .attr \class \color
        ..on \mouseover -> drawOneLayer it if it.reason != "celkem"
        ..on \touchstart -> drawOneLayer it if it.reason != "celkem"
        ..on \mouseout -> drawAllLayers!

      ..exit!remove!
      ..select \div.reason .html (.reason)
      ..select \div.color .style \background-color -> color it.reason

  legendCounts := legend.selectAll "li" .select ".count"
  resetLegend!
